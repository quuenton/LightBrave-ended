#quuenton, codera & kel - drain gang forever.
import discord
import os
from discord.ext import commands
from config import settings
import random
import datetime
import time
import json
import requests
from PIL import Image, ImageDraw, ImageFont
bot = commands.Bot(command_prefix = settings['prefix'])

@bot.event
async def on_member_join(user):
    await user.send("**Hello! Welcome to QPL! Read information!**")
    await user.add_roles(956596297102663690)

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Streaming(name="OnlyFans Translation", url='https://www.youtube.com/channel/UCwipLybn9r7ytnUaq484U1A'))
    print("Bot is ready!")

@bot.command()
async def ben(ctx):
    ben=random.randint(1,5)
    if ben == 1:
        embed=discord.Embed(title="Ask Ben", description=f'Ben`s answer - Yes')
        embed.set_image(url="https://c.tenor.com/6St4vNHkyrcAAAAd/yes.gif")
        embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=bot.user.avatar_url)
        await ctx.send(embed=embed)
    if ben == 2:
        embed=discord.Embed(title="Ask ben", description=f'Ben`s answer - No')
        embed.set_image(url="https://c.tenor.com/x2u_MyapWvcAAAAd/no.gif")
        embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=bot.user.avatar_url)
        await ctx.send(embed=embed)
    if ben == 3:
        embed=discord.Embed(title="Ask ben", description=f'Ben`s answer - Ugh..')
        embed.set_image(url="https://c.tenor.com/aomZLSiXCQ8AAAAC/ugh.gif")
        embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=bot.user.avatar_url)
        await ctx.send(embed=embed)
    if ben == 4:
        embed=discord.Embed(title="Ask ben", description=f'Ben`s answer - Ho, ho, ho!')
        embed.set_image(url="https://c.tenor.com/agrQMQjQTzgAAAAd/talking-ben-laugh.gif")
        embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=bot.user.avatar_url)
        await ctx.send(embed=embed)
    if ben == 5:
        embed=discord.Embed(title="Ask ben", description=f'Ben`s answer - *hangs up*')
        embed.set_image(url="https://c.tenor.com/7j3yFGeMMgIAAAAd/talking-ben-ben.gif")
        embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=bot.user.avatar_url)
        await ctx.send(embed=embed)

@bot.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member: discord.Member, *, reason=None):
    author = ctx.message.author
    if reason==None:
      reason=" no reason provided"
    await ctx.guild.kick(member)
    embed=discord.Embed(title=f'User {member} was kicked for {reason}', description=f'Administrator - {author}')
    await ctx.send(embed=embed)

@bot.command()
async def avatar(ctx, *, member: discord.Member = None):
    if not member:
        member1 = ctx.message.author
        member = ctx.message.author
    ua = member.avatar_url
    embed = discord.Embed(title=f'This user avatar -', timestamp=datetime.datetime.utcnow())
    embed.set_image(url=ua)
    embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=bot.user.avatar_url)
    await ctx.send(embed=embed)

@bot.command(name='ping', pass_context=True)
async def ping(ctx, *, member: discord.Member = None):
   start_time = time.time()
   message = await ctx.send(content=':satellite: Checking ping, wait...')
   end_time = time.time()
   member = ctx.message.author
   embed = discord.Embed(title = '️🎉 Pong!', timestamp=datetime.datetime.utcnow())
   embed.add_field(name='Bot`s ping:', value=str(round(bot.latency * 1000)), inline=True)
   embed.add_field(name='Discord Api`s ping:', value=str(round((end_time - start_time) * 1000)), inline=True)
   embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=member.avatar_url)
   embed.description = 'https://replit.com power.'
   await message.edit(content=None, embed=embed)

@bot.command()
async def cat(ctx):
    response = requests.get('https://api.thecatapi.com/v1/images/search')
    json_data = json.loads(response.text[1:][:-1])
    member = ctx.message.author
    embed = discord.Embed(color=0xff9900, title='Random cat')
    embed.set_image(url=json_data['url'])
    embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=member.avatar_url)
    await ctx.send(embed=embed)

@bot.command()
async def halflife(ctx):
    from PIL import Image, ImageDraw, ImageFont
    if ctx.message.attachments:
        for attach in ctx.message.attachments:
            await attach.save('imageforhalflife.png')
        embed123= discord.Embed( title="Loading an image...", color=discord.Color.green())
        embed123.set_author(name = bot.user.name, icon_url = bot.user.avatar_url)
        embed123.set_footer(text = ctx.author.name, icon_url = ctx.author.avatar_url)
        message = await ctx.send(embed = embed123)
        img = Image.open("imageforhalflife.png")
        width = 1366
        height = 765
        resized_img = img.resize((width, height), Image.ANTIALIAS)
        resized_img.save("new_halflife.png")
        im = Image.open('new_halflife.png')
        watermark = Image.open('halflife.png')
        im.paste(watermark, (0,0), watermark)
        im.save('new_new_halflife.png')
        im.close()
        resized_img.close()
        img.close()
        embed123= discord.Embed(title=":white_check_mark: Successfully, here's your picture!", color=discord.Color.green())
        embed123.set_author(name = bot.user.name, icon_url = bot.user.avatar_url)
        embed123.set_footer(text = ctx.author.name, icon_url = ctx.author.avatar_url)
        file=discord.File('new_new_halflife.png')
        embed123.set_image(url= 'attachment://new_new_halflife.png')
        await message.delete()
        await ctx.send(embed=embed123, file = file)
        os.remove("new_new_halflife.png")
        os.remove("new_halflife.png")
        os.remove("imageforhalflife.png")
    else:
        embed123= discord.Embed( title=":no_entry_sign: Error, send photo", color=discord.Color.red())
        embed123.set_author(name = bot.user.name, icon_url = bot.user.avatar_url)
        embed123.set_footer(text = ctx.author.name, icon_url = ctx.author.avatar_url)
        embed123.set_image(url = 'https://c.tenor.com/ImvPM89IPnsAAAAC/depressed-depression.gif')
        await ctx.send(embed = embed123)

@bot.command()
async def dog(ctx):
  response = requests.get('https://dog.ceo/api/breeds/image/random')
  json_data = json.loads(response.text)
  member = ctx.message.author
  embed = discord.Embed(color=0xff9900, title='Random dog', description='API used - https://dog.ceo/api/breeds/image/random')
  embed.set_image(url=json_data['message'])
  embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=member.avatar_url)
  await ctx.send(embed=embed)

@bot.command()
async def tpdne(ctx):
    response = requests.get('https://fakeface.rest/face/json')
    json_data = json.loads(response.text)
    member = ctx.message.author
    age = json_data['age']
    gender = json_data['gender']
    embed = discord.Embed(color=0xff9900, title='This Person Does Not Exist', description=f'API used - https://fakeface.rest/face/json \n Some info about it - \n Age - {age} \n Gender - {gender} ')
    embed.set_image(url=json_data['image_url'])
    embed.set_footer(text=f'Asked by {ctx.author.name}', icon_url=member.avatar_url)
    await ctx.send(embed=embed)


bot.run(settings['token'])
